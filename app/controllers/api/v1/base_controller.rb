module Api
  module V1
    class BaseController < ApplicationController


      # Avoid CSRF error
      # http://blog.dougpetkanics.com/making-a-post-request-to-a-rails-api-endpoint/
      # http://stackoverflow.com/questions/9362910/rails-warning-cant-verify-csrf-token-authenticity-for-json-devise-requests
      skip_before_filter :verify_authenticity_token #, :if => Proc.new { |c| c.request.format == 'application/json' }

      doorkeeper_for :all
      #before_filter :authenticate_user!

      private
        def current_user
          if doorkeeper_token
            @current_user ||= User.find(doorkeeper_token.resource_owner_id)
          end
        end
    end
  end
end
