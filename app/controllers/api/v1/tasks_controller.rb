module Api
  module V1
    class TasksController < BaseController



      # GET /tasks
      # GET /tasks.json
      def index

        # @tasks = Task.all
        # BMR -- only get for currently logged-in user

        #user = User.find(doorkeeper_token.resource_owner_id)
        #@tasks = current_user.tasks
        @tasks = Task.find_all_by_user_id( current_user.id )

        respond_to do |format|
          format.json { render json: @tasks }
        end
      end

      # GET /tasks/1
      # GET /tasks/1.json
      def show
        # @task = Task.find(params[:id])
       	# BMR -- update to only pull for currently logged-in user
        @task = Task.where(id: params[:id]).where( user_id:  current_user.id )

        respond_to do |format|
          format.json { render json: @task }
        end
      end

      # POST /tasks
      # POST /tasks.json
      def create

        # http://stackoverflow.com/questions/18690196/how-should-i-format-a-json-post-request-to-my-rails-app
        @task = Task.new(params[:task])

        # BMR
        @task.user_id = current_user.id

        respond_to do |format|
          if @task.save
            format.json { render json: @task }
          else
            format.json { render json: @task.errors, status: :unprocessable_entity }
          end
        end

      end


      # PUT /tasks/1
      # PUT /tasks/1.json
      def update
        #@task = Task.find(params[:id])
       	# BMR -- update to only pull for currently logged-in user
        @task = Task.where(id: params[:id]).where( user_id:  current_user.id )

        respond_to do |format|
          if @task.update_attributes(params[:task])
            format.json { head :no_content }
          else
            format.json { render json: @task.errors, status: :unprocessable_entity }
          end
        end
      end

      # DELETE /tasks/1
      # DELETE /tasks/1.json
      def destroy
        begin

          @task = Task.find(params[:id])
          response_msg = :no_content
          if ( @task.user_id == current_user.id )
            @task.destroy
            response_msg = "Delete successful"
          else
            response_msg = "Current user is not authorized to delete this item"
          end

    	rescue ActiveRecord::RecordNotFound
            response_msg = "No item by that id"
        end


        respond_to do |format|
          format.json { render json: response_msg }
        end
     end


    end
  end
end
