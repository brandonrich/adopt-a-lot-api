DeviseApp::Application.routes.draw do


  use_doorkeeper

  get "welcome/index"
  root :to => 'welcome#index'


  namespace :api, defaults: {format: 'json'} do
    namespace :v1 do
      resources :tasks
      resources :listings
      match 'user', to: 'users#show'
    end
  end

  resources :listing_photos



  devise_for :users,
            :path_names => { :sign_in => 'login',
                              :sign_out => 'logout',
                              :password => 'secret',
                              :confirmation => 'verification',
                              :unlock => 'unblock',
                              :registration => 'register',
                              :sign_up => 'cmon_let_me_in' }


end
