# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

brandon = User.create( email: "Brandon.Rich@gmail.com", password: "f00df1ght")
fit = Listing.create( title: "2008 Honda Fit", description: "Awesome Car", end_date: 5.days.from_now, author: brandon)

app = Doorkeeper::Application.create!( name: "Exchange Client", redirect_uri: "http://localhost:3001/auth/exchange/callback")
