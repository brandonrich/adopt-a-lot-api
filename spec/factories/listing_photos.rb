# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :listing_photo do
    image "MyString"
    listing nil
    caption "MyString"
  end
end
