require 'spec_helper'

describe "listing_photos/edit" do
  before(:each) do
    @listing_photo = assign(:listing_photo, stub_model(ListingPhoto))
  end

  it "renders the edit listing_photo form" do
    render

    assert_select "form[action=?][method=?]", listing_photo_path(@listing_photo), "post" do
    end
  end
end
