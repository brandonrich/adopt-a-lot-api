require 'spec_helper'

describe "listing_photos/new" do
  before(:each) do
    assign(:listing_photo, stub_model(ListingPhoto).as_new_record)
  end

  it "renders new listing_photo form" do
    render

    assert_select "form[action=?][method=?]", listing_photos_path, "post" do
    end
  end
end
